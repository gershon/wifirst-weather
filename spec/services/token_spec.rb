require "rails_helper"

describe Token do
  subject { described_class }

  let(:payload) { { 'one' => 'two' } }
  let(:token) { 'eyJhbGciOiJIUzI1NiJ9.eyJvbmUiOiJ0d28ifQ.HTt4Z_5nc9xH34cIVFlO1lHf6VmCg99tttef3kREN_I' }

  describe '.encode' do
    it { expect(subject.encode(payload)).to eq(token) }
  end

  describe '.decode' do
    it { expect(subject.decode(token)).to eq(payload) }
  end
end
