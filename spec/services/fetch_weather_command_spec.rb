require 'rails_helper'

describe FetchWeatherCommand do
  context 'when city is found' do
    let(:query) { 'paris' }

    it "returns truthy success" do
      fetch_weather_command = described_class.call(query)

      expect(fetch_weather_command.success?).to be_truthy
    end

    it "returns non empty results" do
      fetch_weather_command = described_class.call(query)

      expect(fetch_weather_command.result).not_to be_empty
    end
  end

  context 'when city is not found' do
    let(:query) { 'doesnotexist' }

    it "returns falsy success" do
      fetch_weather_command = described_class.call(query)

      expect(fetch_weather_command.success?).to be_falsy
    end
  end
end
