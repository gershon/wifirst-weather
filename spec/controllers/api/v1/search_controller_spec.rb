require 'rails_helper'

describe Api::V1::SearchController, type: :api do
  it 'responds with a 200 status' do
    get "/search?query=paris"

    expect(last_response.status).to eq 200
  end
end
