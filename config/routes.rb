Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get :search, to: "search#index"
      post :auth, to: "auth#create"

      resource :settings, only: [:show, :update]
    end
  end

  get "*path", to: "pages#home"

  root to: "pages#home"
end
