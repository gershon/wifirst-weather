# frozen_string_literal: true

module Api
  module V1
    class SearchController < Api::V1::BaseController
      def index
        fetch_weather_command = FetchWeatherCommand.call(
          params[:query],
          current_user.settings['days']
        )

        if fetch_weather_command.success?
          render json: { results: fetch_weather_command.result }
        else
          render json: { error: fetch_weather_command.errors },
                 status: :bad_request
        end
      end
    end
  end
end
