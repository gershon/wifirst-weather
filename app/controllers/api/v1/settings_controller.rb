# frozen_string_literal: true

module Api
  module V1
    class SettingsController < Api::V1::BaseController
      def show
        render json: { settings: current_user.settings }
      end

      def update
        @user = current_user
        @user.settings = permitted_params

        if @user.save
          render json: { settings: @user.settings }
        else
          render json: { errors: @user.errors }
        end
      end

      private

      def permitted_params
        params.require(:settings).permit(:city, days: [])
      end
    end
  end
end
