import { applyMiddleware, createStore, compose, combineReducers } from "redux";
import createSagaMiddleware from "redux-saga";
import { createReduxHistoryContext, reachify } from "redux-first-history";
import { createBrowserHistory } from "history";

import rootSaga from "../sagas";
import search from "../reducers/search";
import auth from "../reducers/auth";
import settings from "../reducers/settings";

const composeEnhancers = __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const {
  createReduxHistory,
  routerMiddleware,
  routerReducer
} = createReduxHistoryContext({ history: createBrowserHistory() });

const reducers = combineReducers({
  router: routerReducer,
  settings,
  search,
  auth
});

const sagaMiddleware = createSagaMiddleware();
const middlewares = applyMiddleware(routerMiddleware, sagaMiddleware);
const initialState = {};

export const store = createStore(
  reducers,
  initialState,
  composeEnhancers(middlewares)
);

sagaMiddleware.run(rootSaga);

export const history = createReduxHistory(store);

export const reachHistory = reachify(history);
