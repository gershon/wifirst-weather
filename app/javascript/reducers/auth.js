import * as actionTypes from "../constants/actionTypes";

const initialState = {
  userEmail: null,
  loaded: false,
  loading: false,
  token: null,
  error: null,
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_SIGNIN_REQUEST:
      return {
        ...state,
        loading: true
      }
    case actionTypes.AUTH_SIGNIN_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: true
      }
    case actionTypes.AUTH_UNLOAD:
      return {
        ...state,
        loaded: true,
        userEmail: null,
        token: null
      }
    case actionTypes.AUTH_LOAD:
      return {
        ...state,
        loaded: true,
        userEmail: action.payload.userEmail || null,
        token: action.payload.token
      }
    default:
      return state
  }
}

export default authReducer;
