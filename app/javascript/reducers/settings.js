import * as actionTypes from '../constants/actionTypes';

const initialState = {
  loaded: false,
  loading: false,
  days: [],
  city: null
}

const settingsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SETTINGS_LOAD:
      return {
        ...state,
        ...action.payload,
        loaded: true
      }
    case actionTypes.SETTINGS_UNLOAD:
      return {
        ...state,
        days: [],
        city: null
      }
    default:
      return state
  }
}

export default settingsReducer;
