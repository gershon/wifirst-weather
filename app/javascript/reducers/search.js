import * as actionTypes from '../constants/actionTypes';

const initialState = {
  loading: false,
  results: [],
  result: null,
  error: null
}

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_REQUEST:
      return {
        ...state,
        focused: true,
        loading: true
      }
    case actionTypes.SEARCH_REQUEST_SUCCESS:
      return {
        ...state,
        results: action.payload.results,
        loading: false
      }
    case actionTypes.SEARCH_REQUEST_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      }
    default:
      return state
  }
}

export default searchReducer;
