const API_URL = 'http://localhost:5000/api/v1';

const api = {};

api.search = async ({ query, token }) => {
  const opts = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  };

  try {
    const res = await fetch(`${API_URL}/search?query=${query}`, opts);
    const json = await res.json();

    return json;
  } catch(error) {
    return { error: 'Server error' }
  }
}

api.signin = async (payload) => {
  const opts = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload)
  };

  try {
    const res = await fetch(`${API_URL}/auth`, opts);

    const { token, error } = await res.json();

    if (error) {
      return { error };
    }

    return { token };
  } catch(error) {
    return { error: 'Server error' }
  }
}

api.settings = {};

api.settings.update = async ({ payload, token }) => {
  const opts = {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify({ settings: payload })
  };

  try {
    const res = await fetch(`${API_URL}/settings`, opts);
    const { error } = await res.json();

    return { error };
  } catch(error) {
    return { error: 'Server error' }
  }
}

api.settings.fetch = async ({ token }) => {
  const opts = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  };

  try {
    const res = await fetch(`${API_URL}/settings`, opts);
    const { settings } = await res.json();

    return { settings };
  } catch(error) {
    return { error: 'Server error' }
  }
}

export default api;
