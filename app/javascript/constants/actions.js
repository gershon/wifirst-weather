import { createAction } from "redux-actions";
import * as actionTypes from "./actionTypes";

// search
export const searchRequest = createAction(actionTypes.SEARCH_REQUEST, payload => payload);
export const searchRequestFailure = createAction(actionTypes.SEARCH_REQUEST_FAILURE, payload => payload);
export const searchRequestSuccess = createAction(actionTypes.SEARCH_REQUEST_SUCCESS, payload => payload);

// auth
export const authSigninRequest = createAction(actionTypes.AUTH_SIGNIN_REQUEST, payload => payload);
export const authSigninRequestFailure = createAction(actionTypes.AUTH_SIGNIN_REQUEST_FAILURE, payload => payload);
export const authSigninRequestSuccess = createAction(actionTypes.AUTH_SIGNIN_REQUEST_SUCCESS, payload => payload);
export const authLoad = createAction(actionTypes.AUTH_LOAD, payload => payload);
export const authUnload = createAction(actionTypes.AUTH_UNLOAD, payload => payload);
export const authSignout = createAction(actionTypes.AUTH_SIGNOUT, payload => payload);

// settings
export const settingsFetchRequest = createAction(actionTypes.SETTINGS_FETCH_REQUEST, payload => payload);
export const settingsFetchRequestFailure = createAction(actionTypes.SETTINGS_FETCH_REQUEST_FAILURE, payload => payload);
export const settingsFetchRequestSuccess = createAction(actionTypes.SETTINGS_FETCH_REQUEST_SUCCESS, payload => payload);
export const settingsUpdateRequest = createAction(actionTypes.SETTINGS_UPDATE_REQUEST, payload => payload);
export const settingsUpdateRequestFailure = createAction(actionTypes.SETTINGS_UPDATE_REQUEST_FAILURE, payload => payload);
export const settingsUpdateRequestSuccess = createAction(actionTypes.SETTINGS_UPDATE_REQUEST_SUCCESS, payload => payload);
export const settingsLoad = createAction(actionTypes.SETTINGS_LOAD, payload => payload);
export const settingsUnload = createAction(actionTypes.SETTINGS_UNLOAD, payload => payload);
