import { all, takeLatest, select, put } from "redux-saga/effects";
import { LOCATION_CHANGE } from "redux-first-history";
import jwt_decode from "jwt-decode";

import * as actionTypes from "../constants/actionTypes";
import * as actions from "../constants/actions";
import * as search from "./search";
import * as auth from "./auth";
import * as settings from "./settings";

function* route({ payload }) {
  const { auth, settings } = yield select();
  let token;
  let userEmail;

  if (!auth.loaded) {
    token = localStorage.getItem('token');

    let payload;

    try {
      payload = jwt_decode(token);
    } catch(error) {
      payload = {};
    }

    userEmail = payload.user_email

    yield put(actions.authLoad({ token, userEmail }));
  }

  if (token && userEmail && !settings.loaded) {
    yield put(actions.settingsFetchRequest({ token }));
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(LOCATION_CHANGE, route),
    takeLatest(actionTypes.AUTH_SIGNIN_REQUEST, auth.signin),
    takeLatest(actionTypes.AUTH_SIGNOUT, auth.signout),
    takeLatest(actionTypes.SEARCH_REQUEST, search.request),
    takeLatest(actionTypes.SETTINGS_UPDATE_REQUEST, settings.update),
    takeLatest(actionTypes.SETTINGS_FETCH_REQUEST, settings.fetch)
  ])
}
