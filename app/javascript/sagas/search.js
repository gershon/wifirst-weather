import { call, put, select } from "redux-saga/effects";

import * as actions from "../constants/actions";
import api from "../services/api";

export function* request({ payload }) {
  const { auth } = yield select();
  const { token } = auth;
  const { results, error } = yield call(api.search, { query: payload.query, token });

  if (error) {
    yield put(actions.searchRequestFailure({ error }));
  } else {
    yield put(actions.searchRequestSuccess({ results }));
  }
}
