import { call, put } from "redux-saga/effects";
import jwt_decode from "jwt-decode";

import * as actions from "../constants/actions";
import api from "../services/api";
import { reachHistory } from "../store";

export function* signin({ payload }) {
  const { token, error } = yield call(api.signin, payload);

  if (error) {
    const [message] = error.base;

    yield put(actions.authSigninRequestFailure({ error: message }));
  } else {
    localStorage.setItem('token', token);

    const tokenPayload = jwt_decode(token);

    yield put(actions.authLoad({ token, userEmail: tokenPayload.user_email }));
  }
}

export function* signout() {
  localStorage.removeItem('token');

  yield put(actions.authUnload());
  yield put(actions.settingsUnload());

  reachHistory.push('/');
}
