import { call, put, select } from "redux-saga/effects";

import * as actions from "../constants/actions";
import api from "../services/api";

export function* update({ payload }) {
  const { auth } = yield select();
  const { token } = auth;

  const { settings, error } = yield call(api.settings.update, { payload, token });

  if (error) {
    yield put(actions.settingsUpdateRequestFailure({ error }));
  } else {
    yield put(actions.settingsLoad({ ...settings }));
  }
}

export function* fetch({ payload }) {
  const { auth } = yield select();
  const { token } = auth;

  const { settings, error } = yield call(api.settings.fetch, { payload, token });

  if (error) {
    yield put(actions.settingsFetchRequestFailure({ error }));
  } else {
    yield put(actions.settingsLoad({ ...settings }));
  }
}
