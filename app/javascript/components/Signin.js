import React, { Component, createRef } from "react";
import { Link } from "@reach/router";
import { connect } from "react-redux";

import * as actions from "../constants/actions";

class Signin extends Component {
  constructor(props) {
    super(props);

    this.emailInputRef = createRef();
    this.passwordInputRef = createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const email = this.emailInputRef.current.value;
    const password = this.passwordInputRef.current.value;

    this.props.authSigninRequest({ email, password });
  }

  render() {
    return (
      <div className="signin">
        <div className="">{this.props.error}</div>
        <form id="signin">
          <label>Email</label>
          <input ref={this.emailInputRef} placeholder="test@test.com" />

          <label>Password</label>
          <input type="password" ref={this.passwordInputRef} />

          <button onClick={this.handleSubmit}>
            {this.props.loading ? "Signin" : "Signin..."}
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth.loading,
  error: state.auth.error
});

const mapDispatchToProps = dispatch => ({
  authSigninRequest: payload => dispatch(actions.authSigninRequest(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Signin);
