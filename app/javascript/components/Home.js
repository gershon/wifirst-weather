import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import Forecasts from "./Forecasts"

import Search from "./Search";
import Signin from "./Signin";

class Home extends Component {
  render() {
    if (!this.props.auth.loaded) {
      return null;
    }

    return (
      <Fragment>
        {this.props.auth.token ? (
          <Fragment>
            <Search />
            <Forecasts />
          </Fragment>
        ) : (
          <Signin />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Home);
