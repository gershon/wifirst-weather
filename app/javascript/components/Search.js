import React, { Component, createRef } from "react";
import { Link } from "@reach/router";
import { connect } from "react-redux";

import * as actions from "../constants/actions";

class Search extends Component {
  state = {
    query: null
  }

  constructor(props) {
    super(props);

    this.inputRef = createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const query = this.inputRef.current.value;

    if (query) {
      this.props.searchRequest({ query })
    }
  }

  render() {
    return (
      <div className="search">
        <form>
          <input ref={this.inputRef} placeholder="Paris" />

          <button onClick={this.handleSubmit}>
            {this.props.loading ? "Rechercher..." : "Rechercher"}
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.search.loading
});

const mapDispatchToProps = dispatch => ({
  searchRequest: payload => dispatch(actions.searchRequest(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
