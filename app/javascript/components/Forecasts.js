import React from "react";
import { connect } from "react-redux";
import dayjs from "dayjs";

import * as actions from "../constants/actions";

const Forcasts = props => {
  return (
    <div className="forecasts">
      {props.results.length ? (
        props.results.map(result => (
          <div key={result.dt} className="forecasts__item">
            {dayjs(result.dt_txt).format("dddd DD h")}H
            {result.weather.map(weather => (
              <div key={weather.id}className="">{weather.main}</div>
            ))}
          </div>
        ))
      ) : (
        <span>No forecasts to display.</span>
      )}
    </div>
  );
}

const mapStateToProps = state => ({
  results: state.search.results
});

export default connect(mapStateToProps)(Forcasts);
