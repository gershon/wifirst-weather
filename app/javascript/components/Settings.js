import React, { Component, createRef } from "react";
import { Link } from "@reach/router";
import { connect } from "react-redux";

import * as actions from "../constants/actions";
import Checkbox from "./Checkbox";

const days = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
];

class Settings extends Component {
  state = {
    checkedDays: [],
    loaded: false
  }

  constructor(props) {
    super(props);

    this.cityInputRef = createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDaysChange = this.handleDaysChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.loaded && !state.loaded) {
      return { checkedDays: props.days, loaded: true };
    }

    return state;
  }

  handleDaysChange(day) {
    if (this.state.checkedDays.includes(day)) {
      this.setState({ checkedDays: this.state.checkedDays.filter(sday => sday !== day) });
    } else {
      this.setState({ checkedDays: [...this.state.checkedDays, day] });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.settingsUpdateRequest({
      days: [...this.state.checkedDays],
      city: this.cityInputRef.current.value
    });
  }

  render() {
    return (
      <div className="signin">
        <div className="">{this.props.error}</div>
        <form id="settings">
          <label>City</label>
          <input ref={this.cityInputRef} placeholder="Paris" defaultValue={this.props.city} />
          {days.map(day => (
            <div key={day} className="checkbox">
              <label>
                <input
                  type="checkbox"
                  name={day}
                  checked={this.state.checkedDays.includes(day)}
                  onChange={() => this.handleDaysChange(day)}
                />
                {day}
              </label>
            </div>
          ))}
          <button onClick={this.handleSubmit}>
            {this.props.loading ? "Update settings..." : "Update settings"}
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loaded: state.settings.loaded,
  loading: state.settings.loading,
  error: state.settings.error,
  city: state.settings.city,
  days: state.settings.days
});

const mapDispatchToProps = dispatch => ({
  settingsUpdateRequest: payload => dispatch(actions.settingsUpdateRequest(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
