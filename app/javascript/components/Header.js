import React, { Fragment } from "react";
import { Link } from "@reach/router";
import { connect } from "react-redux";

import * as actions from "../constants/actions";

const Header = (props) => {
  return (
    <nav className="nav">
      <Link className="nav__link" to="/">Wifirst</Link>

      {(props.auth.loaded && props.auth.userEmail) && (
        <Fragment>
          <Link className="nav__link" to="settings">Settings</Link>
          <span className="nav__link" onClick={props.authSignout}>
            Signout
          </span>
          <span>{props.auth.userEmail}</span>
        </Fragment>
      )}
    </nav>
  )
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  authSignout: () => dispatch(actions.authSignout())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
