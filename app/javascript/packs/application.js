import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import { LocationProvider, Router } from "@reach/router";

import "../styles/application";

import { store, history, reachHistory } from "../store";
import Header from "../components/Header";
import Home from "../components/Home";
import Settings from "../components/Settings";
import Signin from "../components/Signin";

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(
    <Provider store={store}>
      <LocationProvider history={reachHistory}>
        <Fragment>
          <Header />

          <Router history={reachHistory}>
            <Home path="/" />
            <Settings path="settings" />
            <Signin path="signin" />
          </Router>
        </Fragment>
      </LocationProvider>
    </Provider>,
    document.body.appendChild(document.createElement("div")),
  )
});
