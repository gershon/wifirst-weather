# frozen_string_literal: true

OPENWEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/forecast'
OPENWEATHER_API_KEY = ENV['OPENWEATHERMAP_API_KEY']

class FetchWeatherCommand < BaseCommand
  attr_reader :query, :days

  def initialize(query, days = Date::DAYNAMES)
    @query = query
    @days = days
  end

  def payload
    return unless @query.present?

    response = Rails.cache.fetch("forecasts/#{@query}", expires_in: 1.hour) do
      HTTParty.get("#{OPENWEATHER_API_URL}?q=#{@query}&appid=#{OPENWEATHER_API_KEY}")
    end

    result = JSON.parse response.body, symbolize_names: true

    if result[:cod] == '200'
      if @days.empty?
        @result = result[:list]
        return
      end

      @result = result[:list].select do |r|
        @days.any? { |day| Time.at(r[:dt]).send("#{day.downcase}?") }
      end
    elsif result[:cod] == '404'
      errors.add(:city, 'City not found')
    else
      errors.add(:server, 'Server error')
    end
  end
end
