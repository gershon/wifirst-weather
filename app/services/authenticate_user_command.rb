# frozen_string_literal: true

class AuthenticateUserCommand < BaseCommand
  attr_reader :email, :password

  def initialize(email, password)
    @email = email
    @password = password
  end

  def user
    @user ||= User.find_by(email: email)
  end

  def password_valid?
    user && user.authenticate(password)
  end

  def payload
    if password_valid?
      @result = Token.encode(contents)
    else
      errors.add(:base, 'Invalid credentials')
    end
  end

  def contents
    {
      user_id: user.id,
      user_email: user.email,
      exp: 24.hours.from_now.to_i
    }
  end
end
