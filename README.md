# Wifirst test

## Setup

Requires Postgresql, Redis, Ruby 2.5 and Yarn to be installed.

Copy .env with valid credentials

```
cp .env.example .env
```

Install dependencies and setup database

```
./bin/setup
```

## Run

Start rails application and webpack-dev-server

```
bundle exec foreman s -f Procfile.dev
```

## Test

```
bundle exec rails test
```
